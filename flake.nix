{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    master.url = "github:nixos/nixpkgs/master";
    home-manager = {
        url = "github:rycee/home-manager/master";
        inputs = {
          nixpkgs.follows = "nixpkgs";
        };
      };

   emacs-overlay.url = "github:nix-community/emacs-overlay";
    };
  outputs =  inputs@{ self,unstable,master,emacs-overlay,nixpkgs, ... }:{
      homeConfigurations = {
      cosmos = inputs.home-manager.lib.homeManagerConfiguration {
        system = "x86_64-linux";
        homeDirectory = "/home/vamshi";
        username = "vamshi";
        configuration = { config, lib, pkgs, ... }:
        {
          nixpkgs.overlays =  [inputs.emacs-overlay.overlay];
          nixpkgs.config = { allowUnfree = true; };
          # This value determines the Home Manager release that your
          # configuration is compatible with. This helps avoid breakage
          # when a new Home Manager release introduces backwards
          # incompatible changes.
          #
          # You can update Home Manager without changing this value. See
          # the Home Manager release notes for a list of state version
          # changes in each release.
          # home.stateVersion = "21.11";
          home.keyboard = null;
          # Let Home Manager install and manage itself.
          programs.home-manager.enable = true;
          imports = [
            ./programs/alacritty
            ./programs/command-not-found
            ./programs/emacs
            ./programs/exa
            ./programs/fish
            ./programs/firefox
            ./programs/git
            ./programs/htop
            ./programs/neovim
            ./programs/obs-studio
            ./programs/starship
            ./programs/xmobar
            # ./xsession

            # ./gtk

		        ./services/cbatticon
            ./services/dunst
            ./services/randomBg
            ./services/xcape
          ];
        };
      };
      };
 };
}
