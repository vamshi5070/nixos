{ config, lib, pkgs, ... }:
{
  programs.xmobar = {
    enable = true;
    extraConfig = ''
Config { font    = "xft:Mononoki Nerd Font:weight=bold:pixelsize=16:antialias=true:hinting=true"
       , additionalFonts = [ "xft:Mononoki Nerd Font:pixelsize=11:antialias=true:hinting=true"
                           , "xft:Font Awesome 5 Free Solid:pixelsize=15"
                           , "xft:Font Awesome 5 Brands:pixelsize=19"
                           ]
       , bgColor = "#282c34"
       , fgColor = "#ff6c6b"
       , position = Static { xpos = 0 , ypos = 0, width = 1920, height = 30 }
       , lowerOnStart = True
       , hideOnStart = False
       , allDesktops = True
       , persistent = True
       , iconRoot = "/etc/nixos/programs/xmobar/xpm/"  -- default: "."
       , commands = [
                    -- Time and date
                      Run Date "<fn=2>\xf017</fn> %b %d %Y - (%H:%M) " "date" 50
                      -- Network up and down
                    , Run Network "enp6s0" ["-t", "<fn=2>\xf0ab</fn>  <rx>kb  <fn=2>\xf0aa</fn>  <tx>kb"] 20
                      -- Cpu usage in percent
                    , Run Cpu ["-t", "<fn=2>\xf108</fn>  cpu: (<total>%)","-H","50","--high","red"] 20
                      -- Ram used number and percent
                    , Run Memory ["-t", "<fn=2>\xf233</fn>  mem: <used>M (<usedratio>%)"] 20
                      -- Disk space free
                    , Run DiskU [("/", "<fn=2>\xf0c7</fn>  hdd: <free> free")] [] 60
                      -- Runs custom script to check for pacman updates.
                      -- This script is in my dotfiles repo in .local/bin.
                    , Run Com "/home/dt/.local/bin/pacupdate" [] "pacupdate" 36000
                      -- Runs a standard shell command 'uname -r' to get kernel version
                    , Run Com "uname" ["-r"] "" 3600
                    , Run UnsafeStdinReader
		    , Run Battery [
         	            "-t", "<acstatus> <left>% ",
                        "--",
                        --"-c", "",
                        "-O", "<fn=2> \xf0e7</fn>",
                        "-o", "<fn=2> </fn>",
                        "-h", "<fn=2> </fn>",
                        "-l", "<fn=2> </fn>"
                    ] 10
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = " <icon=haskell_20.xpm/> <fc=#666666>|</fc> %UnsafeStdinReader% }{  <fc=#666666>|</fc><fc=#ff6c6b> <action=`alacritty -e htop`>%memory%</action> </fc><fc=#666666>|</fc><fc=#b3afc2>%battery%</fc><fc=#666666>| </fc><fc=#46d9ff><action=`emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(dt/year-calendar))'`>%date%</action>  </fc>"
       }

    '';
  };
}
