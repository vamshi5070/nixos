import XMonad hiding ( (|||) )
import qualified XMonad.StackSet as W
-- import System.IO ()
import qualified Data.Map        as M
import System.Exit
import System.IO (hPutStrLn,hClose, hFlush, Handle)

import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.GridSelect
import XMonad.Actions.Minimize
import XMonad.Actions.MouseResize
import XMonad.Actions.Navigation2D
import XMonad.Actions.RotSlaves
import XMonad.Actions.Submap
import XMonad.Actions.WithAll

import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ServerMode
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

import XMonad.Layout.Accordion
import qualified XMonad.Layout.BoringWindows as BW
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.Minimize
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Prompt
import XMonad.Prompt.Shell (shellPrompt)

import XMonad.Util.EZConfig
import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.Scratchpad

myStartupHook :: X ()
myStartupHook = do
        spawnOnce "feh --randomize --bg-fill ~/wallpapers/*"  -- feh set random wallpaper"
        --spawnOnce "setxkbmap -option caps:swapescape"
        spawnOnce "systemctl --user start redshift.service"
        spawnOnce "brightnessctl s 1"
        setWMName "LG3D"

myTerminal      = "alacritty"
myEditor = "emacsclient -c -a emacs "  -- Sets emacs as editor for tree select
myBrowser = "firefox"
myMusic = " spotify"
myVidEdit = "flatpak run org.kde.kdenlive"
myScreenRec = "flatpak run com.obsproject.Studio"

myFont = "xft:Source Code Pro  Mono:regular:size=9:antialias=true:hinting=true"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth   = 2

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myModMask       = mod4Mask

myWorkspaces    = ["origin","obs","empty"]

myNormColor   = "#282c34"   -- Border color of normal windows
myFocusColor  = "#46d9ff"   -- Border color of focused windows

shellXPConfig = greenXPConfig {
                               autoComplete      = Nothing--Just 100000    -- set Just 100000 for .1 sec
                               , height            = 80
                               ,promptKeymap = vimLikeXPKeymap
                               ,  position =  CenteredAt {xpCenterY = 0.19 , xpWidth = 0.88}
                               ,  font = "xft:Lucida MAC:size=22"
                               ,defaultPrompter = (\_ -> ">  ")
                               ,fgColor = myFocusColor
                       }

emacsXPConfig = shellXPConfig {
                                defaultPrompter = (\_ -> "Emacs: ")
                                ,defaultText = "emacsclient -c -a emacs "
                               }

okularXPConfig = shellXPConfig {
                               defaultText = "okular "
                               }

emacsList = [("ibuffer",spawn "emacsclient -c -a '' --eval '(ibuffer)'")
            ,("dire ",spawn "emacsclient -c -a '' --eval '(dired nil)'")
            ,("emacs",spawn $ "emacsclient -c -a ''")
            -- ,("open",spawn $ "sudo " ++ myEditor ++ " /etc/nixos/programs/emacs/doom.d/" )
            ,("vterm",spawn "emacsclient -c -a '' --eval '(vterm)'")]

moveToList = [
           ("empty", moveTo Prev EmptyWS)
           ,("nonEmpty", moveTo Prev NonEmptyWS)
           ]

myKeys' = [

           -- ("M1-<Return> p",  namedScratchpadAction myScratchPads "terminal")
          -- ,("S-<Tab> w",spawn myTerminal)
          -- ,("S-<Tab> w n",moveTo Prev NonEmptyWS)--spawn myTerminal)
          -- ,("S-<Tab> w S-n",nextWS)--spawn myTerminal)
 --spawn myTerminal)
           -- ,("M1-S-<Return>", spawn $ "dmenu_run -fn 'Lucida MAC:pixelsize=31' -l 7")--namedScratchpadAction myScratchPads "terminal")
           -- ,("M-<Return>", namedScratchpadAction myScratchPads "terminal")
           -- ,("S-<Backspace> o",kill)
           -- ,("S-<Return> k o",kill)
           -- ,("S-<Return> k a",killAll)
           -- ,("M-q o",kill)
           -- ,("S-<Return> t",  namedScratchpadAction myScratchPads "terminal") --kill)
           -- ,("M-a", makeGrid2 moveToList)
           -- ,("M-2",spawn "scrot")
           -- ,("M-e",  shellPrompt emacsXPConfig )--spawn myEditor)
           -- ,("M-q",makeGrid2 killList)
           -- ,("M-q l", killAll)
           -- ,("M4-e y" , spawn "~/vc/firefox/youtube.sh")
           ("S-<Return> f f",makeGrid1 gridList)
           --,("S-<space>",makeGrid1 gridList)
           -- ,("M1-<Space>",makeGrid1 gridList)
           -- ,("-<Space>  ",)
           -- , ("M5-")
           -- Increase/decrease spacing (gaps)
           , ("M-i", decWindowSpacing 4)         -- Decrease window spacing
           , ("M-d", incWindowSpacing 4)         -- Increase window spacing
           , ("M-S-i", decScreenSpacing 4)         -- Decrease screen spacing
           , ("M-S-d", incScreenSpacing 4)         -- Increase screen spacing
           ,("M-m", spawn "echo Entered Submap | dzen2 -p 2 -fn 'Lucida Mac:pixelsize=22'"
                       >> mySubmap)
            ]

newKeys = [
                ("M1-<Space>  g" ,"google",spawn "~/vc/firefox/google.sh")
                ,("M1-<Space> d" ,"youtube",spawn "~/vc/firefox/youtube.sh")
                ,("M1-<Space> x","exit",makeGrid2 exitList)
                -- ,("M1-<Space> t", "terminal",namedScratchpadAction myScratchPads "terminal")
                ,("M1-<Space> w m", "minimize",withFocused minimizeWindow)
                ,("M1-<Space> w l","maximize",withLastMinimized maximizeWindowAndFocus)
                ,("S-<Return>  g" ,"google",spawn "~/vc/firefox/google.sh")
                ,("S-<Return> d" ,"youtube",spawn "~/vc/firefox/youtube.sh")
                ,("S-<Return> x","exit",makeGrid2 exitList)
                -- ,("S-<Return> t", "terminal",namedScratchpadAction myScratchPads "terminal")
                ,("S-<Return> w m", "minimize",withFocused minimizeWindow)
                ,("S-<Return> w l","maximize",withLastMinimized maximizeWindowAndFocus)
                ,("M-q o" , "kill one",kill)
                ,("M-q l" , "kill all",killAll)
                ,("M-<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
                ,("M-<Space>", "fullscreen",sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
                ,("M-S-<Space>", "with xmobar",sendMessage ToggleStruts)         -- Toggles struts
                ,("M1-o", "shell prompt",shellPrompt shellXPConfig)
                ,("M-<Tab>", "windows",alttab)--windows W.focusDown)
                ,("M1-<Tab>", "traverse windows",rotAllUp)--windows W.focusDown)
                ,("M-a e", "prev empty", moveTo Prev EmptyWS)
                ,("M-a n", "prev non empty",moveTo Prev NonEmptyWS)
                ,("M1-]", "next non empty",moveTo Next NonEmptyWS)
                ,("M1-[",  "prev non empty",moveTo Prev NonEmptyWS)
                ,("M-]", "only nextWs",nextWS)
                ,("M-[", "only prevWs", prevWS)
                ,("M-f f","emacs", makeGrid2 xmonadList)
          ]
keyMapDoc :: String -> X Handle
keyMapDoc name = do
  -- focused screen location/size
  r <- withWindowSet $ return . screenRect . W.screenDetail . W.current

  handle <- spawnPipe $ unwords [ "~/home-manager/xsession/dzen.sh"
                                , name
                                , show (rect_x r)
                                , show (rect_y r)
                                , show (rect_width r)
                                , show (rect_height r)
                                , "purple"       -- key color
                                , "white"        -- cmd color
                                , "Lucida MAC" -- font
                                , "18"           -- line height
                                ]
  return handle

toSubmap :: XConfig l -> String -> [(String, X ())] -> X ()
toSubmap c name m = do
  spawn "echo 'Entered Submap   emacs\t vasmhi' | dzen2  -ta l -l 2 -p 2 -x 222 -y 222 -w 233 -h 223 -fn 'Lucida Mac:pixelsize=22'"
  -- pipe <- keyMapDoc name
  submap $ mkKeymap c m
  -- io $ hClose pipe

mainKeymap c = mkKeymap c $
  [
    ("M1-<Space>",toSubmap c "another keymap" (fillter anotherKeymap'))
  ]

fillter xs = [(x,y) | (x,_,y) <- xs]

anotherKeymap' = [
                  ("v","emacs",spawn myEditor)
                ]

mySubmap = submap . M.fromList $
  [((0, xK_w), spawn "alacritty")
  ]
alttab = goToSelected def {gs_cellheight = 100,gs_cellwidth=390,gs_font="xft:Lucida MAC:size=20",gs_navigate = myNavigation,gs_colorizer = myColorizer'}

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal -- ++ " -n scratchpad"
    findTerm   = className =? "Alacritty"--myTerminal
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnSpotify  =  "spotify"-- ++ " -n scratchpad"
    findSpotify   = className =? "Spotify"--myTerminal
    manageSpotify = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w

redList = [
            ("restart",spawn "systemctl --user restart redshift.service")
            ,("stop",spawn "systemctl --user stop redshift.service")
            ,("open",spawn $  myEditor ++ " /etc/nixos/services/redshift/default.nix")
            ]

xmonadList :: [(String,X())]
xmonadList = [
                ("restart",spawn "xmonad --recompile ; xmonad --restart")
                ,("open",spawn $ myEditor ++ " ~/home-manager/xsession/xmonad.hs")
                ,("stop",io $ exitWith ExitSuccess)
             ]

soundList :: [(String,X())]
soundList = [
                ("decrease",spawn $ "~/vc/sound/decrease.sh")
                ,("increase",spawn $ "~/vc/sound/increase.sh")
            ]

appList = [
            ("emacs",shellPrompt emacsXPConfig)
            ,("okular",shellPrompt okularXPConfig)
            ,("spotify", spawn $ myMusic)
       ]

projectList = [
                ("goto", switchProjectPrompt shellXPConfig)
                ,("throw", shiftToProjectPrompt shellXPConfig)
              ]

exitList :: [(String,X())]
exitList = [
                ("shutdown",spawn "poweroff")
                ,("restart",spawn "reboot")
           ]

flatList :: [(String,X())]
flatList  = [
                ("kdenlive",spawn $ myVidEdit)
                ,("obs",spawn $ myScreenRec)
               ]

nixosList = [
                ("update",spawn "sudo nixos-rebuild switch && notify-send 'Nixos updated'")
                ,("homeManager",spawn $ "firefox https://rycee.gitlab.io/home-manager/options.html")
                ,("store",spawn $ "~/vc/firefox/nixos.sh")
                ,("packages",spawn $ "sudo "++ myEditor ++ "/etc/nixos/programs/default.nix")
                ,("flakes",spawn $ "sudo "++ myEditor ++ "/etc/nixos/flake.nix")
            ]

gridList = [
            ("nothing",spawn "")
           ,("alacritty",namedScratchpadAction myScratchPads "terminal")
           --windows W.focusDown)
           ,("windows", alttab)--goToSelected def {gs_cellheight = 100,gs_cellwidth=400,gs_font="xft:Lucida MAC:size=24"})
           -- ,("spotifyy",namedScratchpadAction myScratchPads "spotify")
           ,("redshift",  makeGrid2 redList)
           ,("xmonad",  makeGrid2  xmonadList)
           ,("code",  makeGrid2  emacsList)
           ,("noBorders",sendMessage $ MT.Toggle NOBORDERS)
           ,("firefox",spawn $ myBrowser)
           ,("exit",  makeGrid2  exitList)
           ,("project",  makeGrid2  projectList)
           -- ,("flatpak",  makeGrid2  flatList)
           ,("sound",makeGrid2 soundList)
           ,("moveTo", makeGrid2 moveToList)
           ,("nixos",makeGrid2 nixosList)
           ,("kill",  makeGrid2  killList)
           ,("google",spawn "~/vc/firefox/google.sh")
           ,("youtube",spawn "~/vc/firefox/youtube.sh")
           ,("fulls", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
           ,("apps" , makeGrid2 appList)
           ,("bluetooth",spawn "pavucontrol ; alacritty -e bluetoothctl")
           ,("layout",makeGrid2 layoutList)
           -- ,("nextLayout",sendMessage NextLayout)
           ,("xshell",shellPrompt shellXPConfig)
           ]

gsconfig1 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 70,
                                                         gs_cellwidth = 150 ,
                                                         gs_navigate = mynavNSearch ,
                                                         gs_bordercolor = myFocusColor,
                                                         gs_font = "xft:Lucida MAC:size=18"}

gsconfig2 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 100,
                                                         gs_cellwidth = 200 ,
                                                         gs_navigate = mynavNSearch,
                                                         gs_font = "xft:Lucida MAC:size=23"}
killList =    [
        ("one",kill)
      , ("all" , killAll)
      ,("workspace", removeWorkspace)
      ]

makeGrid1 = runSelectedAction (gsconfig1 myColorizer)
makeGrid2 = runSelectedAction (gsconfig2 myColorizer)

myColorizer' :: a -> Bool -> X (String,String)
myColorizer' _ True = return ("#46eed9","#000000")
myColorizer' _ False = return ("#000000","#dddddd")


myColorizer :: a -> Bool -> X (String,String)
myColorizer _ True = return ("#46d9ee","#000000")
myColorizer _ False = return ("#000000","#dddddd")

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
 [
   ((shiftMask, xK_space), makeGrid1 gridList )
 ]

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), select)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch navNSearch)--myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_Tab)   , moveNext     >> myNavigation)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         ,((0,xK_space) , select)--setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

mynavNSearch = makeXEventhandler $ shadowWithKeymap navNSearchKeyMap navNSearchDefaultHandler
  where navNSearchKeyMap = M.fromList [
          ((0,xK_Escape) , cancel)
          ,((0,xK_Return)     , select)
          ,((0,xK_space)     , select)
          ,((0,xK_Left)       , move (-1,0) >> mynavNSearch)
          ,((0,xK_Right)      , move (1,0) >> mynavNSearch)
          ,((0,xK_Down)       , move (0,1) >> mynavNSearch)
          ,((0,xK_Up)         , move (0,-1) >> mynavNSearch)
          ,((0,xK_Tab)        , moveNext >> mynavNSearch)
          ,((shiftMask,xK_Tab), movePrev >> mynavNSearch)
          ,((0,xK_BackSpace), transformSearchString (\s -> if (s == "") then "" else init s) >> navNSearch)
          ]
        navNSearchDefaultHandler (_,s,_) = do
          transformSearchString (++ s)
          mynavNSearch

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| magnify
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion

layoutList = [("tall",sendMessage $ JumpToLayout "tall")
             ,("magnify",sendMessage $ JumpToLayout "magnify")
             ,("threeRow",sendMessage $ JumpToLayout "threeRow")
             ]


myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    -- , className =? "spotify"        --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    ] <+> namedScratchpadManageHook myScratchPads

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 1.0

main = do
      xmproc <- spawnPipe "xmobar"
      xmonad  $  additionalNav2DKeys (xK_k, xK_h, xK_j, xK_l)
                                    [(mod4Mask,               windowGo  ),
                                     (mod4Mask .|. shiftMask, windowSwap)]
                                    False
              -- $ addDescrKeys ((mod4Mask, xK_F1), xMessage) myKeys'
              $ ewmh def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormColor,
        focusedBorderColor = myFocusColor,

      -- key bindings
        -- keys               = myKeys,
        keys               = mainKeymap,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = minimize . BW.boringWindows $ showWName' myShowWNameTheme myLayoutHook ,
        manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks,
        handleEventHook    =  serverModeEventHookCmd
                               <+> serverModeEventHook
                               <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                               <+> docksEventHook,
        -- logHook            = myLogHook,
        startupHook        = myStartupHook ,
        logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x
                        , ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#98be65" ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#c792ea" ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#b3afc2" "" . shorten 60     -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,(\xs -> tail $ dropWhile (/=' ') xs)l]++ex++[t]
                        }
    }
           `additionalKeysP` (fillter newKeys) --myKeys'

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Apple garamond:bold:size=70"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#000000"
    , swn_color             = "#FFFFFF"
    }


myKeys1 c = (subtitle "Custom Keys":) $ mkNamedKeymap c $
   [("M-x a", addName "useless message" $ spawn "xmessage foo"),
    ("M-c", sendMessage' Expand)]
    ^++^
   [("<XF86AudioPlay>", spawn "mpc toggle" :: X ()),
    ("<XF86AudioNext>", spawn "mpc next")]
