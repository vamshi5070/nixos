{ config, lib, pkgs, ... }:
{
  gtk  ={
    enable = true;
    iconTheme = {
      name = "Whitesur";
      package = pkgs.whitesur-icon-theme;
    };
    theme = {
      name = "Whitesur";
      package = pkgs.whitesur-gtk-theme;
    };
  };
}
