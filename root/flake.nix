{
  description = "NixOS configuration";

  inputs = {
    # Core dependencies.

     #nixpkgs.url = "github:nixos/nixpkgs/nixos-21.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    master.url = "github:nixos/nixpkgs/master";
    nur.url = "github:nix-community/NUR";
    home-manager = {
      url = "github:rycee/home-manager/master";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

     # Extras
   emacs-overlay.url = "github:nix-community/emacs-overlay";
   nix-doom-emacs.url = "github:vlaci/nix-doom-emacs";

};
outputs =  inputs@{ self,unstable,master, emacs-overlay,home-manager, nixpkgs,... }:
  {
 nixosConfigurations.cosmos = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [ ./configuration.nix
                  ./services/redshift
                ];
    };
  };
}

