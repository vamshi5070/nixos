{ pkgs, config, lib, inputs, ... }:{
programs.starship = {
  enable = true;
	enableFishIntegration = true;
	enableBashIntegration = true;

	 settings = {
          add_newline = false;
                # character.symbol = ".";
                 character = {
         success_symbol = "[➜](bold green)";
         error_symbol = "[✗](bold red) ";
        vicmd_symbol = "[V](bold green) ";
              };
line_break = {
          disabled = true;

        directory = {
          truncation_length = "1";
      };
    package = {
        disabled = true;
    };
  };
};
 };
 }
