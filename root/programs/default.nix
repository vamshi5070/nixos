{ config, lib, pkgs, ... }:
{
  home.packages = with pkgs;[
    kdenlive
    dmenu
    brightnessctl
    okular
    brave
    vlc
    pavucontrol
    libnotify
    neofetch
    scrot
    gimp
    # haskell
    ghc
    clojure
    clojure-lsp
    leiningen
    clj-kondo
  # ((emacsPackagesNgGen emacs).emacsWithPackages (epkgs: [
  #  epkgs.spinner
  # ]))
    # git
    ripgrep
    fd
    spotify

  ];
}
