{ config, lib, pkgs, ... }:

{
  programs.vscode = {
    enable = true;
    # package = pkgs.vscodium;
    extensions = [pkgs.vscode-extensions.vscodevim.vim];
    haskell = {
      enable = true;
      hie = {
        enable = true;
        executablePath = (import /home/vamshi/src/haskell-ide-engine {}).hies + "/bin/hie-wrapper";
      };
    };

  };

}
