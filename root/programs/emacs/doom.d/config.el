;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "vamshi krishna"
      user-mail-address "vamshi5070k@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "Hasklug nerd font mono" :size 25)
     doom-variable-pitch-font (font-spec :family "Hasklug nerd font mono" :size 22)
     doom-big-font (font-spec :family "Hasklug nerd font mono" :size 34))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
;; (setq doom-moonlight-padded-modeline t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; KEYBINDINGS
(map! :leader
      :desc "save"
      "a" #'split-window-horizontally)

;; (map! :leader
;;       :desc ""
;;       "dr" #'run-haskell)

(map! :leader
      :desc "reload"
      "dx" #'haskell-process-reload)

(map! :leader
      :desc "haskell-load-file"
      "dw" #'haskell-process-load-file)
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
;; (map! :leader
;;       :desc "switch"
;;       "da" #(switch-to-buffer (other-buffer (current-buffer) t)))
;;       ;; "da" #'haskell-interactive-switch)


(map! :leader
      :desc "commentOrUncomment"
      "z" #'evilnc-comment-or-uncomment-lines)

;; ** Don't ask to quit
(setq confirm-kill-emacs nil)
;; eshell-aliases
(set-eshell-alias! "up" "sudo nixos-rebuild switch")
(set-eshell-alias! "del"  "sudo nix-collect-garbage -d")


(add-hook 'prog-mode-hook 'pixel-scroll-mode)
(add-hook 'text-mode-hook 'pixel-scroll-mode)

(setq doom-modeline-enable-word-count nil)

;; (setq haskell-process-wrapper-function
;;         (lambda (args) (apply 'nix-shell-command (nix-current-sandbox) args)))
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
;; (use-package pdf-view
;;   :hook (pdf-tools-enabled . pdf-view-midnight-minor-mode)
;;   :hook (pdf-tools-enabled . hide-mode-line-mode)
;;   :config
;;   (setq pdf-view-midnight-colors '("#ABB2BF" . "#282C35")))
;; (defun my-haskell-mode-hook ()
;;   (local-set-key (kbd "C-x C-r") 'inferior-haskell-load-and-run))
;; (add-hook 'haskell-mode-hook 'my-haskell-mode-hook)

(defun my-haskell-repl-mode()
  (split-window-horizontally)
  (run-haskell))
(setq +doom-dashboard-banner-file "emacs-dash.png"
      +doom-dashboard-banner-dir "/home/vamshi/.doom.d/")
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
(add-hook! '+doom-dashboard-mode-hook (hide-mode-line-mode 1) (hl-line-mode -1))
(setq-hook! '+doom-dashboard-mode-hook evil-normal-state-cursor (list nil))

(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))

;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!
;;
(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-chmod
  (kbd "O") 'dired-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; (defun my-haskell-load-and-run ()
;;   "Loads and runs the current Haskell file."
;;   (interactive)
;;   (inferior-haskell-load-and-run inferior-haskell-run-command)
;;   (sleep-for 0 100)
;;   (end-of-buffer))
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))
