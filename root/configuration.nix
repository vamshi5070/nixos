# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

#let
#  compiledLayout = pkgs.runCommand "keyboard-layout" {} ''
#    ${pkgs.xorg.xkbcomp}/bin/xkbcomp ${./keymap/layout.xkb} $out
#  '';
#in

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./stumpper.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

    networking = {
	hostName = "cosmos";   # Define your hostname.
	wireless = {
		 enable = true;  # Enables wireless support via wpa_supplicant.
  		 userControlled.enable = true;  # Enables wireless support via wpa_supplicant.
		 interfaces = ["wlp2s0"];
		 networks.Varikuti.pskRaw = "2f5385967f945b489e113af64fb060b86319cf195898249a8cd0189e63078cbd";  # Enables wireless support via wpa_supplicant.
};
};
 nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
   };
  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

fonts.fonts = with pkgs; [
  ubuntu_font_family
  (nerdfonts.override { fonts = [ "Hasklig" "SourceCodePro" "Mononoki" ]; })
  emacs-all-the-icons-fonts
  font-awesome-ttf

];

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";
  nixpkgs.config.allowUnfree = true;
  # Enable CUPS to print documents.
  # services.printing.enable = true;
# nixpkgs.overlays = [
#          (self: super: { hie-nix = import /home/vamshi/src/hie-nix {}; })
#        ];
  hardware.bluetooth = {
       enable = true;
       powerOnBoot = true;
     };
  # Enable sound.
  sound.enable = true;
  # hardware.pulseaudio.enable = true;
hardware.pulseaudio = {
               enable = true;
               extraModules = [ pkgs.pulseaudio-modules-bt ];
               package = pkgs.pulseaudioFull;
               support32Bit = true; # Steam
               # extraConfig = ''7
               #                # load-module module-bluetooth-policy auto_switch=2
               #                	             # load-module module-switch-on-connect
               #                	             	             # load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1
               #                	             	                            #   '';
               #                	             	                              };

    };
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  services.xserver =
    {
      enable = true;
      # Configure keymap in X11
      layout = "us";
      xkbOptions = "caps:swapescape";
    #  xkbDir = "/etc/nixos/";
      libinput= {
        enable = true;
	touchpad = {
        naturalScrolling = true;      # Enable touchpad support (enabled default in most desktopManager).
		    additionalOptions = ''
                                                                      Option "AccelSpeed" "1.0"        # Mouse sensivity
                                                                      Option "TapButton2" "0"          # Disable two finger tap
                                                                      Option "VertScroll Delta" "-180"  # scroll sensitivity
                                                                      Option "HorizScroll Delta" "-180"
                                                                      Option "FingerLow" "40"          # when finger pressure drops below this value, the driver counts it as a release.
                                                                      Option "FingerHigh" "70"
                                                                      '';
		};
		  };
      windowManager = {
        stumpwm-wrapper.enable = true;
        #exwm.enable = true;
        # xmonad = {
                               # enable = true;
		               # enableContribAndExtras = true;
		        # };
      };

       # displayManager = {
			   # defaultSession = "none+xmonad";
		     # autoLogin.enable = true;
         # autoLogin.user = "vamshi";
       #  sessionCommands = "${pkgs.xorg.xkbcomp}/bin/xkbcomp ${compiledLayout} $DISPLAY";
			   # };
  };


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    xcape
    xorg.xmodmap
    xorg.xkbcomp
    dzen2
    # obs-studio
    pcmanfm
    lxappearance
    kdenlive
    dmenu
    brightnessctl
    okular
    brave
    vlc
    lispPackages.clx-truetype
    pavucontrol
    libnotify
    neofetch
    scrot
    gimp
    # haskell
    ghc
    clojure
    clojure-lsp
    leiningen
    clj-kondo
  # ((emacsPackagesNgGen emacs).emacsWithPackages (epkgs: [
  #  epkgs.spinner
  # ]))
    # git
    ripgrep
    fd
    spotify


    # kdenlive
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
  security.sudo.wheelNeedsPassword = false;
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}

