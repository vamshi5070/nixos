{ pkgs, config, lib, inputs, ... }:{
  services.cbatticon = {
		enable = true;
		# lowLevelPercent = 15;
		# iconType = "notification";
		criticalLevelPercent = 10;
		commandCriticalLevel = ''
					               	notify-send "battery critical!"
						'';           
  };
}
